---
title: "Lettre n°1 du 13 avril 2021"
subtitle: ""
tags: [spip]
draft : false

---



## Nouvelle charte académique "Marianne"

Le logo a évolué !

![Logo académique](/img/logoAC_RENNES.jpg "Info bulle ici")

Il convient de le placer :
* dans l'entête par l'identité du site : /ecrire/?exec=configurer_identite
* comme logo du partenaire académique si vous l'avez sur votre site en pied de page.

## SPIP 3.2.9

Nouvelle version stable mise en place

[Spip.net](https://www.spip.net/fr_rubrique91.html)
